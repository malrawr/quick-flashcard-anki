require 'fileutils'

$CURRENT_DIRECTORY = File.dirname(__FILE__)
$FILE_OUTPUT = "#{$CURRENT_DIRECTORY}" << "/anki-decks/"

trap("SIGINT") {"WARNING! CTRL + C Detected, exiting the program now..." }
#at_exit { puts "Exit Detected!" }

def multi_gets(all_text = "")
    while all_text << STDIN.gets
        return formatter(all_text) if all_text["END"]
    end
end

def formatter(all_text)
    all_text.chomp.gsub(/[\n]/,"<BR>").gsub(/(<BR><END>).*/, "")
end

def create_directory
    FileUtils.mkdir_p($FILE_OUTPUT)
end

def create_deck(front, back, tag)
    open($FILE_OUTPUT << tag, 'a') { |f| f << front << ";" << back << "\n"}
end

def prompt
    puts "Front Side:"
    front = multi_gets
    puts "Back Side:"
    back = multi_gets

    puts "Insert a tag: Here are some existing ones: " << Dir.entries($FILE_OUTPUT).to_s
    tag = gets.chomp

    puts "This is your Anki formatted card:"
    puts "Front: " << front
    puts " Back: " << back
    puts "  Tag: " << tag

    final(front, back, tag)
end


def final(front, back, tag)
    puts "Is this okay? y/n"
    while user_response = gets.chomp.downcase
        case user_response
        when "y"
            puts "Good, all finished!"
            create_directory
            create_deck(front, back, tag)
            exit
        when "n"
            puts "Okay let's try again!"
            prompt
        else
            "Select a correct response."
        end
    end
end

prompt