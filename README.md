# Quick Flashcards

This is a ruby script that let's you create fast flashcards.

## Why make this?

In short I made this so that I can follow the Janki method easily.
The problem with trying to write flashcards all the time is the time and effort involved in making them.
However with this script it's possible to make them quick and easily straight from the terminal.
All that is needed after is to open up Anki and import the file created.

## Usage

It basically prompts you for input for each side of the card. 
You can add new lines also within the prompt. 
If you want to stop feeding lines simply type `<END>`.
After that it asks you for a tag which it uses to append that entry into a new file.


### Issues

* `<BR>` lines not tested
    * Converts new lines `\n` into HTML `<BR>` for Anki. This helps with formatting